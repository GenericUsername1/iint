#include "IintNode.h"

IintNode::IintNode() {
  next = nullptr;
}

IintNode::IintNode(bool data[]) {
  next = nullptr;
  for(int i=0;i<IintNode::SIZE;i++)
    bits[i]=data[i];
}

void IintNode::set(bool data[]) {
  for(int i=0;i<IintNode::SIZE;i++)
    bits[i]=data[i];
}

void IintNode::set(bool val, int k) {
  bits[k]=val;
}

bool* IintNode::get() {
  return bits;
}

bool IintNode::get(int k) {
  return bits[k];
}

void IintNode::setNext(IintNode* ptr) {
  next=ptr;
}

IintNode* IintNode::getNext() {
  return next;
}

bool IintNode::isBlank() {
  for(int i=0;i<SIZE;i++) {
    if(bits[i]) return false;
  }
  return true;
}
