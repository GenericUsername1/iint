#ifndef IINT_NODE_H_
#define IINT_NODE_H_

class IintNode {
public:
  static const int SIZE = 32;
  IintNode();
  IintNode(bool data[]);
  void set(bool data[]);
  void set(bool val,int k);
  bool* get();
  bool get(int k);
  void setNext(IintNode* ptr);
  bool isBlank();
  IintNode* getNext();

private:
  bool bits[SIZE] = { };
  IintNode* next;
};

#include "IintNode.cpp"

#endif
