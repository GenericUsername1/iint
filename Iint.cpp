#include "Iint.h"

//helper function instead of importing math.h to use pow()
int pow(int k, int n) {
  //ints are all that matter for what I'm using it for
  int result=1;
  for(int i=0;i<n;i++) {
    result*=k;
  }
  return result;
}

std::string decAdd(std::string s1,std::string s2) {
  std::string result = "";
  int sum = 0;
  bool carry = 0;
  //putting leading zeroes on smaller string
  int diff=s1.size()-s2.size();
  if(diff>0) {
    s2.insert(0,diff,'0');
  }
  else if(diff<0) {
    s1.insert(0,-diff,'0');
  }
  for(int i=0;i<s1.size();i++) result.append("0");
  //loop in reverse order
  for(int i=s1.size()-1;i>=0;i--) {
    sum = s1[i] + s2[i] - 96 + carry;
    if(sum<10) {
      result[i] = (sum+48);
      carry = 0;
    }
    else {
      result[i] = (sum+38);
      carry = 1;
    }
  }
  if(carry) result.insert(result.begin(),'1');
  return result;
}

void Iint::removeBlankNodes() {
  IintNode* curr;
  IintNode* prev;
  while(numNodes>1) {
    IintNode* curr = head;
    IintNode* prev = head;
    curr = curr->getNext();
    //go to last node, such that curr is at last and prev is 2nd to last
    while(curr->getNext()!=nullptr) {
      curr = curr->getNext();
      prev = prev->getNext();
    }
    if(curr->isBlank()) {
      numNodes--;
      prev->setNext(nullptr);
      delete curr;
      curr = nullptr;
    }
    else break;
  }
}

Iint::Iint() {
  head = new IintNode();
  numNodes = 1;
}

Iint::Iint(int k) {
  bool data[IintNode::SIZE];
  //reduce to binary
  int i=0;
  int prev=32;
  int power;
  while(k>0) {
    power=pow(2,i);
    if((power>k) or i>=31) {
      k-=pow(2,i-1);
      //need to set 0s from i to previous i
      for(int j=i;j<prev;j++) {
        data[j]=0;
      }
      data[i-1]=1;
      prev=i-1;
      i=-1;
    }
    i++;
  }
  for(int i=0;i<prev;i++) {
    data[i]=0;
  }
  head = new IintNode(data);
  numNodes = 1;
}

Iint::Iint(const Iint& copy) {
  numNodes = copy.numNodes;
  IintNode* origPtr= copy.head;
  if(origPtr == nullptr) {
    head=nullptr;
  }
  else {
    head = new IintNode(origPtr->get());
    IintNode* newPtr = head;
    origPtr = origPtr->getNext();
    while(origPtr!=nullptr) {
      IintNode* newNode = new IintNode(origPtr->get());
      newPtr->setNext(newNode);
      newPtr=newPtr->getNext();
      origPtr = origPtr->getNext();
    }
    newPtr->setNext(nullptr);
  }
}

std::string Iint::bin() {
  IintNode* ptr = head;
  std::string result = "";
  while(ptr!=nullptr) {
    for(int i=0;i<IintNode::SIZE;i++) {
      result.insert(result.begin(),ptr->get(i)+48);
    }
    ptr=ptr->getNext();
  }
  //get rid of leading zeroes
  while(result[0]=='0') {
    result.erase(result.begin()+0);
  }
  return result;
}

std::string Iint::dec() {
  IintNode* currPtr = head;
  std::string temp = "1";
  std::string result = "0";
  while(currPtr!=nullptr) {
    for(int i=0;i<IintNode::SIZE;i++) {
      if(currPtr->get(i)) {
        result = decAdd(result,temp);
      }
      temp = decAdd(temp,temp);
    }
    currPtr = currPtr->getNext();
  }
  return result;
}

Iint operator+(const Iint& first, const Iint& second) {
  Iint result=Iint();
  IintNode* blank = new IintNode();
  IintNode* resultPtr = result.head;
  IintNode* firstPtr = first.head;
  IintNode* secondPtr = second.head;
  bool store[IintNode::SIZE];
  bool carry=0;
  bool next=0;
  bool a;
  bool b;
  while((firstPtr!=nullptr)||(secondPtr!=nullptr)||(carry)) {
    if(firstPtr==nullptr) firstPtr = blank;
    if(secondPtr==nullptr) secondPtr = blank;
    for(int i=0;i<IintNode::SIZE;i++) {
      a = firstPtr->get(i);
      b = secondPtr->get(i);
      next = ((a && b)||(b && carry)||(a && carry));
      resultPtr->set((((a == b)&&(b==carry))==next),i);
      carry = next;
    }
    firstPtr=firstPtr->getNext();
    secondPtr=secondPtr->getNext();
    if((firstPtr!=nullptr)||(secondPtr!=nullptr)||(carry)) {
      result.numNodes++;
      IintNode* newNode = new IintNode();
      resultPtr->setNext(newNode);
      resultPtr=resultPtr->getNext();
    }
  }
  return result;
}

Iint operator*(const Iint& first, const Iint& second) {
  Iint result = Iint();
  Iint temp = Iint();
  IintNode* tempPtr = temp.head;
  IintNode* curr;
  IintNode* firstPtr = first.head;
  IintNode* secondPtr = second.head;
  int space = 0;
  int index;
  while(secondPtr!=nullptr) {
    for(int i=0;i<IintNode::SIZE;i++) {
      if(secondPtr->get(i)) {
        //Add in space number of zeroes
        index = space % IintNode::SIZE;
        for(int j=1;j<((int)space/IintNode::SIZE);j++) {//should run zero times if space<32
          temp.numNodes++;
          IintNode* newNode = new IintNode();
          tempPtr->setNext(newNode);
          tempPtr = tempPtr->getNext();
        }
        tempPtr=temp.head;
        firstPtr = first.head;
        while(firstPtr!=nullptr){
          for(int j=0;j<IintNode::SIZE;j++) {
            tempPtr->set(firstPtr->get(j),index);
            index++;
            if(index>=IintNode::SIZE) {
              temp.numNodes++;
              IintNode* newNode = new IintNode();
              tempPtr->setNext(newNode);
              tempPtr = tempPtr->getNext();
              index=0;
            }
          }
          firstPtr = firstPtr->getNext();
        }
        temp.removeBlankNodes();
        result = result + temp;
        //delete temp, resetting initial conditions
        tempPtr = temp.head;
        while(tempPtr!=nullptr) {
          curr = tempPtr;
          tempPtr = tempPtr->getNext();
          curr->setNext(nullptr);
          delete curr;
        }
        temp.head = new IintNode();
        tempPtr = temp.head;
      }
      space++;
    }
    secondPtr = secondPtr->getNext();
  }
  return result;
}
