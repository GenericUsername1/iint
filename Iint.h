#ifndef IINT_H_
#define IINT_H_

#include "IintNode.h"
#include <string>

class Iint {
public:
  Iint();
  Iint(int k);
  //consider string initializer
  Iint(const Iint& copy);
  std::string bin();//display in binary
  std::string dec();//display in decimal
  friend Iint operator+(const Iint& first, const Iint& second);
  friend Iint operator*(const Iint& first, const Iint& second);
  //~Iint(); Removed because it was breaking things

private:
  void removeBlankNodes();
  IintNode* head;
  int numNodes;

};

#include "Iint.cpp"

#endif
