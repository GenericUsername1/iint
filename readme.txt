Indefinite Integer Data Type

Integer that allocates more memory as it expands.

Current Functionality:
1) Addition 
2) Multiplication
3) Binary Display
4) Decimal Display
5) Integer Constructor

To be added:
1) Subtraction
2) Integer Division
3) Negatives (unary operator)
4) Assignment Operators
5) Comparison Operators
6) String Constructor
7) Other math functions? (may be done in separate library)