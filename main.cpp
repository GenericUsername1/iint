#include "Iint.h"
#include <iostream>

using namespace std;

Iint fib(int n) {
  if(n==0) return 0;
  if(n==1) return 1;
  Iint prev = 0;
  Iint curr = 1;
  Iint result;
  for(int i=1;i<n;i++) {
    result = prev + curr;//destructor called here
    prev = curr;
    curr = result;
  }
  return result;
}

int main() {
  /*
  int n;
  cout << "Fibonacci: " << endl;
  cin >> n;
  Iint result = fib(n);
  cout << "fib(" << n << ") = " << result.dec() << endl;
  */
  Iint x = 111111111;
  Iint y = 111111111;
  Iint z = x * y;
  cout << z.dec() << endl;
}
